#include "Application.h"
#include <iostream>

#define PI  3.1516
void Application::update()
{

}

void Application::draw()
{
	int x = 0, y = 0, x0 = 250, y0 = 250;
	int alpha;
	for (alpha = 0; alpha < 360; ++alpha)
	{
		x = (100 * (float)cos(alpha * PI / 180)) + x0;
		y = (100 * (float)sin(alpha * PI / 180)) + y0;
		if (alpha <= 90 && alpha >= 0)
			linea(x0, y0, x, y);
		else if (alpha <= 180 && alpha >= 0)
			linea(x, y, x0, y0);
		else
			linea(y, x, x0, y0);

	}





}

void Application::linea(int x0, int y0, int x, int y)
{
	int dy = (y - y0), dx = (x - x0), E = 2 * dy, NE = 2 * dy - 2 * dx;
	int d = (2 * dy) - dx;

	putPixel(x0, y0, 255, 255, 0, 255);
	int w = x0, z = y0;
	while (w <= x)
	{
		if (d > 0)
		{
			++z;
			d += NE;
		}
		else
		{
			d += E;
		}
		putPixel(w, z, 255, 255, 0, 255);
		putPixel(z, w, 255, 255, 0, 255);



		++w;

	}

}